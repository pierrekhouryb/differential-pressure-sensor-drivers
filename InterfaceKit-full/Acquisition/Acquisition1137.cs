﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InterfaceKit_full.Acquisition
{
    public partial class Acquisition1137 : AcquisitionBase
    {
        private List<float> _timeStamp = new List<float>();
        private List<float> _data = new List<float>();
        private List<float> _dataPsi = new List<float>();

        public Acquisition1137()
        {
            InitializeComponent();
        }

        public override void ChangeValue(int val)
        {            
            //double tmp = ((double)val / 18.0) - 27.777; // Sensor 1126
            double tmp = (val / 57.143) - 8.75; // Sensor 1137
            this.textBoxData.Text = tmp.ToString("0.####");
            _data.Add((float)tmp);

            //tmp = ((double)val * 0.008055) - 4.0277; // Sensor 1126
            tmp = (val / 394.09) - 1.269; // Sensor 1137
            this.textBoxDataPsi.Text = tmp.ToString("0.####");
            _dataPsi.Add((float)tmp);

            double time = getTimeNowInSeconds() - _timerAnchorInSeconds;
            _timeStamp.Add((float)time);
        }

        public override void ResetAcquisition()
        {
            //Clear textboxes
            _data.Clear();
            _dataPsi.Clear();
            _timeStamp.Clear();
            this.textBoxData.Clear();
            this.textBoxDataPsi.Clear();
            //Re-init
            startTimer();
            _data.Add(0f);
            _dataPsi.Add(0f);
            _timeStamp.Add(0f);
        }
        
        public override List<List<float>> GetAllData()
        {
            return new List<List<float>>
            {
                _timeStamp,
                _data,
                _dataPsi
            };
        }
    }
}
