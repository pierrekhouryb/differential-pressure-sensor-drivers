﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InterfaceKit_full.Acquisition
{
    public partial class Acquisition0000 : AcquisitionBase
    {
        private List<float> _timeStamp = new List<float>();
        private List<float> _data = new List<float>();

        public Acquisition0000()
        {
            InitializeComponent();
        }

        public override void ChangeValue(int val)
        {
            this.textBoxData.Text = val.ToString("0.#");
            _data.Add((float)val);
            

            double time = getTimeNowInSeconds() - _timerAnchorInSeconds;
            _timeStamp.Add((float)time);
        }

        public override void ResetAcquisition()
        {
            //Clear textboxes
            _data.Clear();
            _timeStamp.Clear();
            this.textBoxData.Clear();
            //Re-init
            startTimer();
            _data.Add(0f);
            _timeStamp.Add(0f);
        }

        public override List<List<float>> GetAllData()
        {
            return new List<List<float>>
            {
                _timeStamp,
                _data
            };
        }
    }
}
