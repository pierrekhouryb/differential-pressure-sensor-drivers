﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.IO;

namespace InterfaceKit_full
{
    public partial class AcquisitionForm : Form
    {
        private bool _isRunning = false;
        private string _report;

        private AcquisitionForm()
        {
            InitializeComponent();
            buttonStop.Enabled = false;
        }

        public AcquisitionForm(string sensorType): this()
        {
            switch (sensorType)
            {
                default:
                case "Sensor0000":
                    this.acquisitionBase1 = (Acquisition.Acquisition0000)this.acquisitionBase1;
                    break;
                case "Sensor1137":
                    this.acquisitionBase1 = (Acquisition.Acquisition1137)this.acquisitionBase1;
                    break;
            }
        }

        public void ChangeValue(int val)
        {
            if(_isRunning)
            {
                this.acquisitionBase1.ChangeValue(val);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            _isRunning = true;
            buttonStart.Enabled = false;
            buttonStop.Enabled = true;
            buttonSave.Enabled = false;
            this.acquisitionBase1.ResetAcquisition();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            _isRunning = false;
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
            buttonSave.Enabled = true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            _report = this.acquisitionBase1.FormatDataToString();
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk_1(object sender, CancelEventArgs e)
        {
            string name = saveFileDialog1.FileName;
            File.WriteAllText(name, _report);
        }        
    }
}
