﻿namespace InterfaceKit_full.Acquisition
{
    partial class Acquisition1137
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDataPsi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxData = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(471, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 36);
            this.label2.TabIndex = 10;
            this.label2.Text = "psi";
            // 
            // textBoxDataPsi
            // 
            this.textBoxDataPsi.Font = new System.Drawing.Font("Courier New", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDataPsi.Location = new System.Drawing.Point(4, 196);
            this.textBoxDataPsi.Name = "textBoxDataPsi";
            this.textBoxDataPsi.Size = new System.Drawing.Size(457, 143);
            this.textBoxDataPsi.TabIndex = 9;
            this.textBoxDataPsi.Text = "0.0";
            this.textBoxDataPsi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(471, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 36);
            this.label1.TabIndex = 8;
            this.label1.Text = "kPa";
            // 
            // textBoxData
            // 
            this.textBoxData.Font = new System.Drawing.Font("Courier New", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxData.Location = new System.Drawing.Point(4, 1);
            this.textBoxData.Name = "textBoxData";
            this.textBoxData.Size = new System.Drawing.Size(457, 143);
            this.textBoxData.TabIndex = 7;
            this.textBoxData.Text = "0.0";
            this.textBoxData.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Acquisition1137
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxDataPsi);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxData);
            this.Name = "Acquisition1137";
            this.Size = new System.Drawing.Size(541, 342);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxDataPsi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxData;
    }
}
