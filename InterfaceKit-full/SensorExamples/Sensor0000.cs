using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace InterfaceKit_full.SensorExamples
{
    public partial class Sensor0000: UserControl
    {
        public int sensorValue;
        AcquisitionForm _form;
        string _sensorType = "Sensor0000";

        public Sensor0000()
        {
            InitializeComponent();
        }

        public void changeDisplay(int val)
        {
            textBox1.Text = val.ToString("") + "V";
            if (_form != null)
                _form.ChangeValue(val);
        }

        private void buttonAcquisition_Click(object sender, EventArgs e)
        {
            _form = new AcquisitionForm(_sensorType);
            _form.Show();
        }
    }
}
