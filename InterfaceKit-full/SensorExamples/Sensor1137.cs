using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace InterfaceKit_full.SensorExamples
{
    public partial class Sensor1137: UserControl
    {
        public int sensorValue;
        AcquisitionForm _form;
        string _sensorType = "Sensor1137";

        public Sensor1137()
        {
            InitializeComponent();
        }

        public void changeDisplay(int val)
        {
            double tmp = (val / 57.143) - 8.75;
            textBox1.Text = tmp.ToString("0.####") + "kPa";
            tmp = (val / 394.09) - 1.269;
            textBox2.Text = tmp.ToString("0.####") + "psi";
            if (_form != null)
                _form.ChangeValue(val);
        }

        private void buttonAcquisition_Click(object sender, EventArgs e)
        {
            _form = new AcquisitionForm(_sensorType);
            _form.Show();
        }
    }
}
